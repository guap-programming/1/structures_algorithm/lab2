package main

import (
	"fmt"
	doublyLinkedList "gitlab.com/otus_golang/05_doublylinkedlist" // эта библиотека написана мной
	"math"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	floats := make([]float64, 0, 10)
	for i := 0; i < 10; i++ {
		floats = append(floats, math.Round(rand.NormFloat64()*100)/100)
	}

	whole := doublyLinkedList.List{}
	decimal := doublyLinkedList.List{}

	fmt.Print("numbers: ")
	for _, number := range floats {
		if number == math.Trunc(number) {
			fmt.Printf("\nnumber %f is int\n", number)
			continue
		}
		fmt.Print(number, " ")
		i, f := math.Modf(number)
		whole.PushBack(i)
		decimal.PushBack(math.Round(f*100) / 100)
	}

	wholeNum := whole.First()
	fmt.Print("\nwholes:  ")

	for i := 0; i < int(whole.Len())-1; i++ {
		fmt.Print(wholeNum.Value, "    ")
		wholeNum = wholeNum.Next()
	}

	fractionalNum := decimal.First()
	fmt.Print("\ndecimal: ")

	for i := 0; i < int(decimal.Len())-1; i++ {
		fmt.Print(fractionalNum.Value, " ")
		fractionalNum = fractionalNum.Next()
	}

}
